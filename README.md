# UptimeRobot PHP SDK

This library provides an API Client for [UptimeRobot API](https://uptimerobot.com/api/).

## Installation

```
composer require wizbii/uptimerobot-php-sdk
```

## Usage

### Basic

```php
$apiKey = "abcd"; // replace it with your own key
$client = new \Wizbii\UptimeRobotPHPSDK\UptimeRobotHttpClient($apiKey);
$monitors = $client->getAllMonitors();
```

### Symfony

```shell
# .env
UPTIMEROBOT_APIKEY=abcd
```

```yaml
# config/services.yaml
parameters:
    uptimerobot.apikey: '%env(UPTIMEROBOT_APIKEY)%'
    env(UPTIMEROBOT_APIKEY): 'abcd'

services:
    Wizbii\UptimeRobotPHPSDK\UptimeRobotHttpClient:
        arguments:
              $apiKey: '%uptimerobot.apikey%'
```

```php
# any services like a controller
namespace App\Controller;

use Wizbii\UptimeRobotPHPSDK\UptimeRobotHttpClient;

class MyController
{
    public function __construct(
        private readonly UptimeRobotHttpClient $uptimeRobotHttpClient
    ) {}
    
    public function myFunction(): void
    {
        $monitors = $this->uptimeRobotHttpClient->getAllMonitors();
        // do something
    }
}
```

## API Methods

All API methods are documented and strongly typed in `Wizbii\UptimeRobotPHPSDK\UptimeRobotHttpClient`

## Contribute

Steps : 
- clone or fork the repository
- create a branch
- do your development
- check that everything is fine using `composer dev:checks`
- create a Merge Request