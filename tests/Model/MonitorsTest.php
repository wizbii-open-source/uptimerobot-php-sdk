<?php

namespace Tests\Wizbii\UptimeRobotPHPSDK\Model;

use PHPUnit\Framework\TestCase;
use Wizbii\UptimeRobotPHPSDK\Model\AlertContacts;
use Wizbii\UptimeRobotPHPSDK\Model\Enum\MonitorType;
use Wizbii\UptimeRobotPHPSDK\Model\Monitor;
use Wizbii\UptimeRobotPHPSDK\Model\Monitors;

class MonitorsTest extends TestCase
{
    public function test_it_can_filter_by_friendly_name()
    {
        $monitors = new Monitors([
            $this->getMonitor(1, 'name-a'),
            $this->getMonitor(2, 'name-b'),
            $this->getMonitor(3, 'name-a'),
        ]);
        $monitor = $monitors->getMonitorWithFriendlyName('name-a');
        $this->assertThat($monitor->getId(), $this->equalTo(1));
    }

    private function getMonitor(int $id, string $friendlyName): Monitor
    {
        return new Monitor(
            $id,
            $friendlyName,
            'https://example.org',
            MonitorType::Http,
            30,
            new AlertContacts([])
        );
    }
}
