<?php

namespace Tests\Wizbii\UptimeRobotPHPSDK\Model;

use PHPUnit\Framework\TestCase;
use Wizbii\UptimeRobotPHPSDK\Model\AlertContacts;
use Wizbii\UptimeRobotPHPSDK\Model\Enum\MonitorType;
use Wizbii\UptimeRobotPHPSDK\Model\Monitor;

class MonitorTest extends TestCase
{
    public function test_it_can_deserialize()
    {
        $monitor = Monitor::deserialize([
            'id' => 1234,
            'friendly_name' => 'My Homepage',
            'url' => 'https://www.example.org',
            'type' => 1,
            'interval' => 30,
        ]);
        $this->assertThat($monitor->getId(), $this->equalTo(1234));
        $this->assertThat($monitor->getFriendlyName(), $this->equalTo('My Homepage'));
        $this->assertThat($monitor->getUrl(), $this->equalTo('https://www.example.org'));
        $this->assertThat($monitor->getType(), $this->equalTo(MonitorType::Http));
        $this->assertThat($monitor->getInterval(), $this->equalTo(30));
    }

    public function test_it_can_get_creation_request_body()
    {
        $monitor = new Monitor(null, 'My Homepage', 'https://www.example.org', MonitorType::Http, 30, new AlertContacts([]));
        $requestParameters = $monitor->asCreationRequest();
        $this->assertThat($requestParameters['friendly_name'], $this->equalTo('My Homepage'));
        $this->assertThat($requestParameters['url'], $this->equalTo('https://www.example.org'));
        $this->assertThat($requestParameters['type'], $this->equalTo(1));
        $this->assertThat($requestParameters['interval'], $this->equalTo(30));
        $this->assertThat($requestParameters, $this->logicalNot($this->arrayHasKey('id')));
    }

    public function test_it_can_get_update_request_body()
    {
        $monitor = new Monitor(1234, 'My Homepage', 'https://www.example.org', MonitorType::Http, 30, new AlertContacts([]));
        $requestParameters = $monitor->asUpdateRequest();
        $this->assertThat($requestParameters['id'], $this->equalTo(1234));
        $this->assertThat($requestParameters['friendly_name'], $this->equalTo('My Homepage'));
        $this->assertThat($requestParameters['url'], $this->equalTo('https://www.example.org'));
        $this->assertThat($requestParameters['type'], $this->equalTo(1));
        $this->assertThat($requestParameters['interval'], $this->equalTo(30));
    }
}
