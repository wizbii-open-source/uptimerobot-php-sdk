<?php

namespace Tests\Wizbii\UptimeRobotPHPSDK;

use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Component\HttpClient\Response\MockResponse;
use Wizbii\UptimeRobotPHPSDK\Model\Monitor;
use Wizbii\UptimeRobotPHPSDK\UptimeRobotHttpClient;

class UptimeRobotHttpClientTest extends TestCase
{
    public function test_it_can_get_a_list_of_monitors()
    {
        $expectedRequests = [
            function ($method, $url, $options) {
                $this->assertThat($method, $this->equalTo('POST'));
                $this->assertThat($url, $this->stringEndsWith('getMonitors'));
                parse_str($options['body'], $params);
                $this->assertThat($params['limit'], $this->equalTo(2));
                $this->assertThat($params['offset'], $this->equalTo(0));

                return new MockResponse(
                    '{"stat":"ok","pagination":{"offset":0,"limit":2,"total":3},
                        "monitors":[
                            {"id":1,"friendly_name":"Monitor A","url":"https://www.example.org/a","type":1,"sub_type":"","keyword_type":null,"keyword_case_type":0,"keyword_value":"","http_username":"","http_password":"","port":"","interval":60,"timeout":30,"status":2,"create_datetime":1605780124},
                            {"id":2,"friendly_name":"Monitor B","url":"https://www.example.org/b","type":1,"sub_type":"","keyword_type":null,"keyword_case_type":0,"keyword_value":"","http_username":"","http_password":"","port":"","interval":60,"timeout":30,"status":2,"create_datetime":1605780124}
                         ]
                     }'
                );
            },
            function ($method, $url, $options) {
                $this->assertThat($method, $this->equalTo('POST'));
                $this->assertThat($url, $this->stringEndsWith('getMonitors'));
                parse_str($options['body'], $params);
                $this->assertThat($params['limit'], $this->equalTo(2));
                $this->assertThat($params['offset'], $this->equalTo(2));

                return new MockResponse(
                    '{"stat":"ok","pagination":{"offset":2,"limit":2,"total":3},
                        "monitors":[
                            {"id":3,"friendly_name":"Monitor C","url":"https://www.example.org/c","type":1,"sub_type":"","keyword_type":null,"keyword_case_type":0,"keyword_value":"","http_username":"","http_password":"","port":"","interval":60,"timeout":30,"status":2,"create_datetime":1605780124}
                         ]
                     }'
                );
            },
        ];
        $client = new UptimeRobotHttpClient('abcd', 2);
        $client->injectHttpClient(new MockHttpClient($expectedRequests));
        $monitors = $client->getAllMonitors();
        $this->assertThat($monitors, $this->countOf(3));
    }

    public function test_it_can_get_a_single_monitor()
    {
        $expectedRequests = [
            function ($method, $url, $options) {
                $this->assertThat($method, $this->equalTo('POST'));
                $this->assertThat($url, $this->stringEndsWith('getMonitors'));
                parse_str($options['body'], $params);
                $this->assertThat($params['limit'], $this->equalTo(1));
                $this->assertThat($params['monitors'], $this->equalTo(1));

                return new MockResponse(
                    '{"stat":"ok","pagination":{"offset":0,"limit":1,"total":1},
                        "monitors":[
                            {"id":1,"friendly_name":"Monitor A","url":"https://www.example.org/a","type":1,"sub_type":"","keyword_type":null,"keyword_case_type":0,"keyword_value":"","http_username":"","http_password":"","port":"","interval":60,"timeout":30,"status":2,"create_datetime":1605780124}
                         ]
                     }'
                );
            },
        ];
        $client = new UptimeRobotHttpClient('abcd', 2);
        $client->injectHttpClient(new MockHttpClient($expectedRequests));
        $monitor = $client->getMonitor(1);
        $this->assertThat($monitor, $this->isInstanceOf(Monitor::class));
    }
}
