<?php

namespace Wizbii\UptimeRobotPHPSDK\Model;

use Wizbii\UptimeRobotPHPSDK\Model\Enum\StatusPageSort;
use Wizbii\UptimeRobotPHPSDK\Model\Enum\StatusPageStatus;

class StatusPage
{
    /**
     * @param int[] $monitors
     */
    public function __construct(
        private readonly ?int $id,
        private readonly string $friendlyName,
        private array $monitors,
        private readonly StatusPageSort $statusPageSort,
        private readonly StatusPageStatus $statusPageStatus,
        private readonly ?string $standardUrl,
        private readonly ?string $customUrl,
    ) {
    }

    public static function deserialize(mixed $entity): self
    {
        return new self(
            $entity['id'],
            $entity['friendly_name'],
            $entity['monitors'],
            StatusPageSort::from($entity['sort']),
            StatusPageStatus::from($entity['status']),
            $entity['standard_url'],
            $entity['custom_url']
        );
    }

    /** @param int[] $monitors */
    public static function getNewOne(string $friendlyName, array $monitors, ?string $customUrl = null): StatusPage
    {
        return new StatusPage(
            id: null,
            friendlyName: $friendlyName,
            monitors: $monitors,
            statusPageSort: StatusPageSort::Alphabetically,
            statusPageStatus: StatusPageStatus::Active,
            standardUrl: null,
            customUrl: $customUrl
        );
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFriendlyName(): string
    {
        return $this->friendlyName;
    }

    /**
     * @return int[]
     */
    public function getMonitors(): array
    {
        return $this->monitors;
    }

    public function getStatusPageSort(): StatusPageSort
    {
        return $this->statusPageSort;
    }

    public function getStatusPageStatus(): StatusPageStatus
    {
        return $this->statusPageStatus;
    }

    public function getStandardUrl(): ?string
    {
        return $this->standardUrl;
    }

    public function getCustomUrl(): ?string
    {
        return $this->customUrl;
    }

    /**
     * @return array<string, mixed>
     */
    public function asCreationRequest(): array
    {
        return [
            'friendly_name' => $this->friendlyName,
            'monitors' => join('-', $this->monitors),
            // 'status' => $this->statusPageStatus->value,
            'sort' => $this->statusPageSort->value,
            'custom_domain' => $this->customUrl,
            'hide_url_links' => 'true',
        ];
    }

    /**
     * @return array<string, mixed>
     */
    public function asUpdateRequest(): array
    {
        return array_merge(
            $this->asCreationRequest(),
            ['id' => $this->id]
        );
    }

    public function withId(int $id): self
    {
        return new self(
            $id,
            $this->friendlyName,
            $this->monitors,
            $this->statusPageSort,
            $this->statusPageStatus,
            $this->standardUrl,
            $this->customUrl
        );
    }

    /** @param int[] $monitors */
    public function withMonitors(array $monitors): StatusPage
    {
        return new self(
            $this->id,
            $this->friendlyName,
            $monitors,
            $this->statusPageSort,
            $this->statusPageStatus,
            $this->standardUrl,
            $this->customUrl
        );
    }
}
