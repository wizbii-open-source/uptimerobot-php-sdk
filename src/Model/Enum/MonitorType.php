<?php

namespace Wizbii\UptimeRobotPHPSDK\Model\Enum;

enum MonitorType: int
{
    case Http = 1;
    case Keyword = 2;
    case Ping = 3;
    case Port = 4;
    case Heartbeat = 5;
}
