<?php

namespace Wizbii\UptimeRobotPHPSDK\Model\Enum;

enum StatusPageSort: int
{
    case Alphabetically = 1;
    case Alphabetically_Reverted = 2;
    case Up_Down_Paused = 3;
    case Down_Up_Paused = 4;
}
