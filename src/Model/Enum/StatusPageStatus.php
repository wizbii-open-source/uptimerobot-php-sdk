<?php

namespace Wizbii\UptimeRobotPHPSDK\Model\Enum;

enum StatusPageStatus: int
{
    case Paused = 0;
    case Active = 1;
}
