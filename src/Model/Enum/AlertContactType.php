<?php

namespace Wizbii\UptimeRobotPHPSDK\Model\Enum;

enum AlertContactType: int
{
    case SMS = 1;
    case Email = 2;
    case Twitter = 3;
    case Web_Hook = 5;
    case Push_Bullet = 6;
    case Zapier = 7;
    case Pro_SMS = 8;
    case Push_Over = 9;
    case Slack = 11;
    case Voice_Call = 14;
    case Splunk = 15;
    case Pager_Duty = 16;
    case Ops_Genie = 17;
    case MS_Teams = 20;
    case Google_Chat = 21;
    case Discord = 23;
}
