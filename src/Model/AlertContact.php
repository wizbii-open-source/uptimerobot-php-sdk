<?php

namespace Wizbii\UptimeRobotPHPSDK\Model;

use Wizbii\UptimeRobotPHPSDK\Model\Enum\AlertContactType;

class AlertContact
{
    public function __construct(
        private readonly int $id,
        private readonly ?string $friendlyName,
        private readonly AlertContactType $alertContactType,
        private readonly string $value,
        private readonly ?int $threshold,
        private readonly ?int $recurrence,
    ) {
    }

    public static function deserialize(mixed $entity): self
    {
        return new AlertContact(
            $entity['id'],
            $entity['friendly_name'] ?? null,
            AlertContactType::from($entity['type']),
            $entity['value'],
            $entity['threshold'] ?? null,
            $entity['recurrence'] ?? null,
        );
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getAlertContactType(): AlertContactType
    {
        return $this->alertContactType;
    }

    public function getFriendlyName(): ?string
    {
        return $this->friendlyName;
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function asText(): string
    {
        return join('_', [$this->id, $this->threshold, $this->recurrence]);
    }
}
