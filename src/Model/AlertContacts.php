<?php

namespace Wizbii\UptimeRobotPHPSDK\Model;

use ArrayIterator;
use Countable;
use IteratorAggregate;
use Traversable;

/**
 * @implements \IteratorAggregate<AlertContact>
 */
class AlertContacts implements Countable, IteratorAggregate
{
    /** @param AlertContact[] $alertContacts */
    public function __construct(
        private array $alertContacts
    ) {
    }

    public function getIterator(): Traversable
    {
        return new ArrayIterator($this->alertContacts);
    }

    public function count(): int
    {
        return count($this->alertContacts);
    }

    public function asText(): string
    {
        return join('-', array_map(fn (AlertContact $alertContact) => $alertContact->asText(), $this->alertContacts));
    }
}
