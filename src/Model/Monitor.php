<?php

namespace Wizbii\UptimeRobotPHPSDK\Model;

use Wizbii\UptimeRobotPHPSDK\Model\Enum\MonitorType;

class Monitor
{
    public const TIMEOUT_SHORTEST = 60;
    public const TIMEOUT_FIVE_MINUTES = 300;
    public const TIMEOUT_HOURLY = 3600;

    public function __construct(
        private readonly ?int $id,
        private readonly string $friendlyName,
        private readonly string $url,
        private readonly MonitorType $type,
        private readonly int $interval,
        private readonly AlertContacts $alertContacts
    ) {
    }

    public static function deserialize(mixed $entity): self
    {
        return new Monitor(
            $entity['id'],
            $entity['friendly_name'],
            $entity['url'],
            MonitorType::from($entity['type']),
            $entity['interval'],
            new AlertContacts(array_map(fn (array $config) => AlertContact::deserialize($config), $entity['alert_contacts'] ?? []))
        );
    }

    public static function getNewHttpMonitor(string $url, string $friendlyName, int $interval = self::TIMEOUT_FIVE_MINUTES): self
    {
        return new Monitor(
            id: null,
            url: $url,
            friendlyName: $friendlyName,
            type: MonitorType::Http,
            interval: $interval,
            alertContacts: new AlertContacts([])
        );
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFriendlyName(): string
    {
        return $this->friendlyName;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function getType(): MonitorType
    {
        return $this->type;
    }

    public function getInterval(): int
    {
        return $this->interval;
    }

    /**
     * @return array<string, mixed>
     */
    public function asCreationRequest(): array
    {
        return [
            'friendly_name' => $this->friendlyName,
            'url' => $this->url,
            'type' => $this->type->value,
            'interval' => $this->interval,
            'alert_contacts' => $this->alertContacts->asText(),
        ];
    }

    /**
     * @return array<string, mixed>
     */
    public function asUpdateRequest(): array
    {
        return array_merge(
            $this->asCreationRequest(),
            ['id' => $this->id]
        );
    }

    public function withId(int $id): self
    {
        return new Monitor(
            $id,
            $this->friendlyName,
            $this->url,
            $this->type,
            $this->interval,
            $this->alertContacts
        );
    }

    public function withUrl(string $url): self
    {
        return new Monitor(
            $this->id,
            $this->friendlyName,
            $url,
            $this->type,
            $this->interval,
            $this->alertContacts
        );
    }

    public function withAlertContacts(AlertContacts $alertContacts): self
    {
        return new Monitor(
            $this->id,
            $this->friendlyName,
            $this->url,
            $this->type,
            $this->interval,
            $alertContacts
        );
    }
}
