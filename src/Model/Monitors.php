<?php

namespace Wizbii\UptimeRobotPHPSDK\Model;

use ArrayIterator;
use Countable;
use IteratorAggregate;
use RuntimeException;
use Traversable;

/**
 * @implements \IteratorAggregate<Monitor>
 */
class Monitors implements Countable, IteratorAggregate
{
    /** @param Monitor[] $monitors */
    public function __construct(
        private array $monitors
    ) {
    }

    public function getIterator(): Traversable
    {
        return new ArrayIterator($this->monitors);
    }

    public function count(): int
    {
        return count($this->monitors);
    }

    /** @param int[] $monitorIds */
    public function filterByMonitorIds(array $monitorIds): Monitors
    {
        return new Monitors(
            array_values(
                array_filter($this->monitors, fn (Monitor $monitor) => in_array($monitor->getId(), $monitorIds))
            )
        );
    }

    public function first(): Monitor
    {
        if (empty($this->monitors)) {
            throw new RuntimeException('can\'t get first element from monitors as it is empty');
        }

        return $this->monitors[0];
    }

    public function add(Monitor $monitor): Monitors
    {
        $this->monitors[] = $monitor;

        return $this;
    }

    public function replace(int $monitorId, Monitor $updatedMonitor): Monitors
    {
        $monitors = [];
        foreach ($this->monitors as $monitor) {
            $monitors[] = ($monitor->getId() !== $monitorId) ? $monitor : $updatedMonitor;
        }

        $this->monitors = $monitors;

        return $this;
    }

    public function getMonitorWithFriendlyName(string $friendlyName): ?Monitor
    {
        $matches = array_values(array_filter($this->monitors, fn (Monitor $monitor) => $monitor->getFriendlyName() === $friendlyName));

        return empty($matches) ? null : $matches[0];
    }

    public function getMonitorByUrl(string $url): ?Monitor
    {
        $matches = array_values(array_filter($this->monitors, fn (Monitor $monitor) => $monitor->getUrl() === $url));

        return empty($matches) ? null : $matches[0];
    }
}
