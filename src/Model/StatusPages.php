<?php

namespace Wizbii\UptimeRobotPHPSDK\Model;

use ArrayIterator;
use Countable;
use IteratorAggregate;
use RuntimeException;
use Traversable;

/**
 * @implements \IteratorAggregate<StatusPage>
 */
class StatusPages implements Countable, IteratorAggregate
{
    /** @param StatusPage[] $statusPages */
    public function __construct(
        private readonly array $statusPages
    ) {
    }

    public function getIterator(): Traversable
    {
        return new ArrayIterator($this->statusPages);
    }

    public function count(): int
    {
        return count($this->statusPages);
    }

    public function filterByFriendlyName(string $friendlyName): StatusPages
    {
        return new StatusPages(
            array_values(
                array_filter($this->statusPages, fn (StatusPage $statusPage) => $statusPage->getFriendlyName() === $friendlyName)
            )
        );
    }

    public function first(): StatusPage
    {
        if (empty($this->statusPages)) {
            throw new RuntimeException('can\'t get first element from statusPages as it is empty');
        }

        return $this->statusPages[0];
    }

    public function getStatusPageWithFriendlyName(string $friendlyName): ?StatusPage
    {
        return ($f = $this->filterByFriendlyName($friendlyName))->count() > 0 ? $f->first() : null;
    }
}
