<?php

namespace Wizbii\UptimeRobotPHPSDK;

use RuntimeException;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Wizbii\UptimeRobotPHPSDK\Model\AlertContact;
use Wizbii\UptimeRobotPHPSDK\Model\AlertContacts;
use Wizbii\UptimeRobotPHPSDK\Model\Monitor;
use Wizbii\UptimeRobotPHPSDK\Model\Monitors;
use Wizbii\UptimeRobotPHPSDK\Model\StatusPage;
use Wizbii\UptimeRobotPHPSDK\Model\StatusPages;

class UptimeRobotHttpClient
{
    private HttpClientInterface $httpClient;

    public function __construct(
        private readonly string $apiKey,
        private readonly int $limitPerCall = 50
    ) {
    }

    public function injectHttpClient(HttpClientInterface $httpClient): self
    {
        $this->httpClient = $httpClient;

        return $this;
    }

    private function getHttpClient(): HttpClientInterface
    {
        if (!isset($this->httpClient)) {
            $this->httpClient = HttpClient::create();
        }

        return $this->httpClient;
    }

    /**
     * @param array<string, mixed> $parameters
     *
     * @return array<string, mixed>
     *
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    private function makeRequest(string $path, array $parameters): array
    {
        $response = $this->getHttpClient()->request('POST', "https://api.uptimerobot.com/v2/$path", array_merge_recursive($parameters, ['body' => ['api_key' => $this->apiKey]]));
        if ($response->getStatusCode() > 300) {
            throw new RuntimeException("Cannot make request to $path. Status code is ".$response->getStatusCode());
        }

        return json_decode($response->getContent(), true);
    }

    /** @api */
    public function getAllMonitors(): Monitors
    {
        $monitors = [];
        $offset = 0;
        do {
            $newMonitors = $this->getMonitors($offset, $this->limitPerCall);
            $monitors = array_merge($monitors, $newMonitors);
            $isDone = count($newMonitors) < $this->limitPerCall;
            $offset += $this->limitPerCall;
        } while (!$isDone);

        return new Monitors($monitors);
    }

    /** @api */
    public function getMonitor(string $monitorId): ?Monitor
    {
        $response = $this->makeRequest('getMonitors', [
            'body' => [
                'monitors' => $monitorId,
                'limit' => 1,
            ],
        ]);
        if (empty($response['monitors'])) {
            return null;
        }

        return Monitor::deserialize($response['monitors'][0]);
    }

    /** @return Monitor[] */
    private function getMonitors(int $offset, int $limit): array
    {
        $monitors = [];
        $entities = $this->makeRequest('getMonitors', [
            'body' => [
                'offset' => $offset,
                'limit' => $limit,
                'alert_contacts' => 1,
            ],
        ]);
        foreach ($entities['monitors'] as $entity) {
            $monitors[] = Monitor::deserialize($entity);
        }

        return $monitors;
    }

    /** @api */
    public function createMonitor(Monitor $monitor): Monitor
    {
        $response = $this->makeRequest('newMonitor', ['body' => $monitor->asCreationRequest()]);

        return $monitor->withId((int) $response['monitor']['id']);
    }

    /** @api */
    public function deleteMonitor(Monitor $monitor): void
    {
        $this->makeRequest('deleteMonitor', ['body' => ['id' => $monitor->getId()]]);
    }

    /** @api */
    public function updateMonitor(Monitor $monitor): Monitor
    {
        $response = $this->makeRequest('editMonitor', ['body' => $monitor->asUpdateRequest()]);

        return $monitor->withId((int) $response['monitor']['id']);
    }

    /** @api */
    public function getAllAlertContacts(): AlertContacts
    {
        $alertContacts = [];
        $offset = 0;
        do {
            $newAlertContacts = $this->getAlertContacts($offset, $this->limitPerCall);
            $alertContacts = array_merge($alertContacts, $newAlertContacts);
            $isDone = count($newAlertContacts) < $this->limitPerCall;
            $offset += $this->limitPerCall;
        } while (!$isDone);

        return new AlertContacts($alertContacts);
    }

    /** @return AlertContact[] */
    private function getAlertContacts(int $offset, int $limit): array
    {
        $alertContacts = [];
        $entities = $this->makeRequest('getAlertContacts', [
            'body' => [
                'offset' => $offset,
                'limit' => $limit,
            ],
        ]);
        foreach ($entities['alert_contacts'] as $entity) {
            $alertContacts[] = AlertContact::deserialize($entity);
        }

        return $alertContacts;
    }

    /** @api */
    public function getAllStatusPages(): StatusPages
    {
        $statusPages = [];
        $offset = 0;
        do {
            $newStatusPages = $this->getStatusPages($offset, $this->limitPerCall);
            $statusPages = array_merge($statusPages, $newStatusPages);
            $isDone = count($newStatusPages) < $this->limitPerCall;
            $offset += $this->limitPerCall;
        } while (!$isDone);

        return new StatusPages($statusPages);
    }

    /** @return StatusPage[] */
    private function getStatusPages(int $offset, int $limit): array
    {
        $statusPages = [];
        $entities = $this->makeRequest('getPSPs', [
            'body' => [
                'offset' => $offset,
                'limit' => $limit,
            ],
        ]);
        foreach ($entities['psps'] as $entity) {
            $statusPages[] = StatusPage::deserialize($entity);
        }

        return $statusPages;
    }

    /** @api */
    public function createStatusPage(StatusPage $statusPage): StatusPage
    {
        $response = $this->makeRequest('newPSP', ['body' => $statusPage->asCreationRequest()]);

        return $statusPage->withId((int) $response['psp']['id']);
    }

    /** @api */
    public function updateStatusPage(StatusPage $statusPage): StatusPage
    {
        $response = $this->makeRequest('editPSP', ['body' => $statusPage->asUpdateRequest()]);

        return $statusPage->withId((int) $response['psp']['id']);
    }
}
